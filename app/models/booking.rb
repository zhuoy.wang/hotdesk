class Booking < ActiveRecord::Base
  belongs_to :user
  belongs_to :desk

  validates :start_at, :end_at, overlap: { scope: 'desk_id', message_content: 'Conflict!!!' }
end
