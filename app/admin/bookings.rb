ActiveAdmin.register Booking do
  permit_params :start_at, :end_at, :user_id, :desk_id
end
