class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.datetime :start_at
      t.datetime :end_at
      t.references :user, index: true, foreign_key: true
      t.references :desk, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
