require 'rails_helper'

RSpec.describe Booking, type: :model do
	it "is accessable" do
		booking = Booking.create!
		expect(booking).to eq(Booking.last)
	end

	it "has username, deskname and booked time columns" do
		columns = Booking.column_names
		expect(columns).to include("id")
		expect(columns).to include("start_at")
		expect(columns).to include("end_at")
		expect(columns).to include("user_id")
		expect(columns).to include("desk_id")
		expect(columns).not_to include("From/To")
	end
end