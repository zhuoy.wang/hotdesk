require 'rails_helper'

RSpec.describe BookingsController, type: :controller do 
  # before(:all) do
  # 	@desk1 = Desk.create(name: "A1")
  # 	@desk2 = Desk.create(name: "A2")
  # 	@booking_1 = Booking.create(start_at: "Sat, 30 Mar 2019 08:12:00 UTC +00:00", end_at: "Sat, 30 Mar 2019 08:12:00 UTC +00:00", user_id: 2, desk_id: 1)
  # 	@booking_2 = Booking.create(start_at: "Sat, 30 Mar 2019 08:24:00 UTC +00:00", end_at: "Sun, 31 Mar 2019 08:12:00 UTC +00:00", user_id: 1, desk_id: 2)
  # end

  it "#index" do
  	get :index
  	expect(response).to have_http_status(200)
  	expect(response).to render_template(:index)
  end

  it "#new" do
  	get :new
  	expect(response).to have_http_status(200)
  	expect(response).to render_template(:new)
  end

  # it "#edit" do
  # 	get :edit, id: @booking_1[:id]
  # 	expect(response).to have_http_status(200)
  # 	expect(response).to render_template(:edit)
  # end

  # describe '#create' do
  # 	before(:all) do
  # 		@booking_params = {start_at: "Sat, 30 Mar 2019 08:24:00 UTC +00:00", end_at: "Sun, 31 Mar 2019 08:12:00 UTC +00:00", user_id: 1, desk_id: 2}
  # 	end

  # 	it "creates record" do
  # 		expect{ post :create, booking: @booking_params }.to change{Booking.all.size}.by(1)
  # 	end

  # 	it "redirect on success" do
  # 		post :create, booking: @post_params
  # 		expect(response).not_to have_http_status(200)
		# expect(response).to have_http_status(302)
		# expect(response).to redirect_to(booking_path(Booking.last))
  # 	end
  # end
end