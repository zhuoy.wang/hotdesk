require "rails_helper"

RSpec.describe "bookings/index.html.erb", type: :view do 
	login_user 

  it 'can render' do
  	@user = User.create(name: "john", email: "john@admin.com", password: "password123", password_confirmation: 'password123')
  	@desk = Desk.create(name: "A1")
  	@booking = Booking.new(start_at:"Sat, 30 Mar 2019 08:24:00 UTC +00:00", end_at: "Sun, 31 Mar 2019 08:12:00 UTC +00:00")
  	@booking.user_id = @user.id
  	@booking.desk_id = @desk.id
  	@booking.save

  	@bookings = Array.new(2, @booking)
  	render
  	expect(rendered).to include("Name")
  	expect(rendered).to include("john")
	end
end